#include "widget.h"
#include "vulkanwindow.h"

#include <QGuiApplication>
#include <QVulkanInstance>
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(lcVk, "qt.vulkan")

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));

    //! [0]
        QVulkanInstance inst;
        inst.setLayers({ "VK_LAYER_KHRONOS_validation" });
        if (!inst.create())
            qFatal("Failed to create Vulkan instance: %d", inst.errorCode());
    //! [0]

    //! [1]
        VulkanWindow w;
        w.setVulkanInstance(&inst);

        w.resize(1024, 768);
        w.show();
    //! [1]

    return a.exec();
}
