#ifndef VULKANWINDOW_H
#define VULKANWINDOW_H

#include <QVulkanWindow>
#include "vulkanrenderer.h"

class VulkanWindow : public QVulkanWindow
{
public:
    QVulkanWindowRenderer *createRenderer() override { return new VulkanRenderer(this); }
};

#endif // VULKANWINDOW_H
